import {Component, OnDestroy, OnInit} from '@angular/core';
import {EncounterRosterService} from "../../../../features/party-roster/services/encounter-roster.service";
import {Actor} from "../../../../features/party-roster/model/actor";
import {Subscription} from "rxjs";

@Component({
    selector: 'app-roster',
    templateUrl: './roster.component.html',
    styleUrls: ['./roster.component.scss']
})
export class RosterComponent implements OnInit, OnDestroy {
    private _actors: Actor[] = [];
    private roster$: Subscription | undefined;
    selectedActor: Actor | undefined;

    constructor(
        private rosterService: EncounterRosterService,
    ) {
    }

    ngOnInit(): void {
        const roster$ = this.rosterService.roster$;
        this._actors = roster$.getValue();
        this.roster$ = roster$.subscribe(roster => {
            this._actors = roster;
            console.log('new roster')
        });
    }

    ngOnDestroy() {
        this.roster$?.unsubscribe();
    }

    get turn(): number {
        return this.rosterService.turn;
    }

    get round(): number {
        return this.rosterService.round;
    }

    get partyName(): string {
        return this.rosterService.partyName;
    }

    get actors(): Actor[] {
        this._actors.sort((a, b) => {
            if (a.initiative !== b.initiative) {
                return b.initiative - a.initiative;
            } else if (!Number.isNaN(+a.modifier) && !Number.isNaN(+b.modifier)) {
                return +b.modifier - +a.modifier;
            } else {
                return a.modifier !== '-' ? -1 : 1;
            }
        });
        return this._actors;
    }

    nextTurn = () => this.rosterService.nextTurn();
    prevTurn = () => this.rosterService.prevTurn();

    removeActor(actor: Actor): void {
        this.rosterService.removeActor(actor.name);
    }

    newEncounter(): void {
        this.rosterService.newEncounter();
    }
}
