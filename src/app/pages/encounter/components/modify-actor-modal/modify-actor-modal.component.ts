import {Component, ElementRef, Input, ViewChild} from '@angular/core';
import {AbstractControl, UntypedFormBuilder, Validators} from "@angular/forms";
import {EncounterRosterService} from "../../../../features/party-roster/services/encounter-roster.service";
import {Actor, validTags} from "../../../../features/party-roster/model/actor";

@Component({
    selector: 'app-modify-actor-modal',
    templateUrl: './modify-actor-modal.component.html',
    styleUrls: ['./modify-actor-modal.component.scss']
})
export class ModifyActorModalComponent {
    actorForm = this.formBuilder.group({
        name: ['', Validators.required],
        initiative: ['', Validators.required],
        modifier: ['', Validators.required],
        isPc: [],
        tag: ['']
    });
    submitted = false;
    validTags = validTags;
    @Input() actor: Actor | undefined;
    @ViewChild('closeButton') closeButton!: ElementRef;

    constructor(
        private formBuilder: UntypedFormBuilder,
        private rosterService: EncounterRosterService,
    ) {
        document.addEventListener('show.bs.modal', () => this.fillIn());
    }

    get actorFormControls(): { [p: string]: AbstractControl<any, any> } {
        return this.actorForm.controls;
    }

    fillIn(): void {
        if (this.actor !== undefined) {
            this.actorForm.patchValue({
                name: this.actor.name,
                initiative: this.actor.initiative,
                modifier: this.actor.modifier,
                isPc: this.actor.isPc
            });
        } else {
            this.actorForm.patchValue({
                name: '',
                initiative: '',
                modifier: '',
                isPc: ''
            });
        }
    }

    onSubmit() {
        this.submitted = true;
        if (this.actorForm.invalid) {
            return;
        }

        const name = this.actorForm.get('name')?.value;
        const initiative = this.actorForm.get('initiative')?.value;
        const modifier = this.actorForm.get('modifier')?.value;
        const isPc = this.actorForm.get('isPc')?.value;

        if (this.actor) {
             this.rosterService.editActor(this.actor.name, name, initiative, modifier);
        } else {
             this.rosterService.addActor(name, initiative, modifier, isPc);
        }
        this.closeButton.nativeElement.click()
    }
}
