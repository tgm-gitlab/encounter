import { TestBed } from '@angular/core/testing';

import { EncounterRosterService } from './encounter-roster.service';

describe('PartyRosterService', () => {
  let service: EncounterRosterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EncounterRosterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
