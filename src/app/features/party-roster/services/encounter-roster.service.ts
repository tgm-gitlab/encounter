import {Injectable, OnDestroy} from '@angular/core';
import {Actor, Mod} from "../model/actor";
import {FailureReason, RosterOperation} from "../model/roster-operation";
import {BehaviorSubject} from "rxjs";

type Optional<T> = T | undefined;

@Injectable({
    providedIn: 'root'
})
export class EncounterRosterService implements OnDestroy {
    private _roster: Actor[] = [];
    private readonly _roster$: BehaviorSubject<Actor[]>;
    private _partyName = '';
    private _round = 1;
    private _turn = 0;

    constructor() {
        this._roster$ = new BehaviorSubject<Actor[]>(this._roster);
        this.addActor('Bob', 10, 3, true);
        this.addActor('Steve', 11, 3, true);
        this.addActor('Goblin 1', 11, 2, false);
        this.addActor('Goblin 2', 8, 2, false);
        this.addActor('Warg', 2, '-', false);
        this._partyName = 'Bob Time';
    }

    ngOnDestroy() {
        this._roster$.complete();
    }

    public loadRoster(partyName: string): void {
        const loaded: Actor[] = [] // magic here
        this._partyName = partyName
        this._roster = loaded
        this._roster$.next(this._roster);
    }

    public get roster$(): BehaviorSubject<Actor[]> {
        return this._roster$;
    }

    public get partyName(): string {
        return this._partyName;
    }

    public addActor(name: string, initiative: number, modifier: number | string, isPc = false): RosterOperation {
        console.log('new act')
        if (!name && this._roster.find(act => act.name === name) === undefined) {
            return FailureReason.name;
        }
        if (!initiative || Number.isNaN(+initiative)) {
            return FailureReason.initiative;
        }
        if (!modifier || (Number.isNaN(+modifier) && !['-', '+'].includes(modifier as string))) {
            console.log(`${!modifier} || ${Number.isNaN(+modifier) && !['-', '+'].includes(modifier as string)}: [${modifier}]`)
            return FailureReason.modifier;
        }
        let _mod: Mod;
        if (modifier === '-' || modifier === '+') {
            _mod = modifier;
        } else {
            _mod = +modifier;
        }


        this._roster.push(new Actor(name, +initiative, _mod, isPc));
        this._roster$.next(this._roster);
        return null;
    }

    public editActor(nameOrPosition: number | string, newName: Optional<string> = undefined,
                     initiative: Optional<number> = undefined,
                     modifier: Optional<Mod> = undefined): RosterOperation {
        console.log('edit act')
        let actor = this._findActor(nameOrPosition);

        if (!actor) {
            return FailureReason.unknown_actor;
        }

        if (newName !== undefined) {
            actor.name = newName;
        }

        if (initiative !== undefined) {
            actor.initiative = initiative;
        }

        if (modifier !== undefined) {
            actor.modifier = modifier;
        }

        const index = this._roster.indexOf(actor);
        this._roster[index] = actor;
        this._roster$.next(this._roster);

        return null;
    }

    private _findActor(nameOrPosition: number | string) {
        return typeof nameOrPosition === 'number' ? this._roster[nameOrPosition] : this._roster.find(act => act.name === nameOrPosition);
    }

    public removeActor(nameOrPosition: number | string): RosterOperation {
        let actor = this._findActor(nameOrPosition);

        if (!actor) {
            return FailureReason.unknown_actor;
        }
        const index = this._roster.indexOf(actor);
        this._roster.splice(index, 1);
        this._roster$.next(this._roster);
        return null;
    }

    public prevTurn() {
        if ((this.turn - 1) >= 0) {
            this._turn -= 1;
        } else {
            this._turn = 0;
            this._round -= 1;
        }
    }

    public nextTurn() {
        if ((this._turn + 1) < this._roster.length) {
            this._turn += 1;
        } else {
            this._turn = 0;
            this._round += 1;
        }
    }

    public get turn(): number {
        return this._turn;
    }

    public get round(): number {
        return this._round;
    }

    public newEncounter(): void {
        const npcs = this._roster.filter(act => !act.isPc);
        npcs.forEach(npc => this.removeActor(npc.name));
        this._round = 1;
        this._turn = 0;
        this._roster$.next(this._roster);
    }
}
