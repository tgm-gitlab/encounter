export type RosterOperation = null | FailureReason;

export enum FailureReason {
  name,
  initiative,
  modifier,
  unknown_actor
}
const reasons = [
    'name',
    'initiative',
    'modifier',
    'unknown_actor'
];

export const rosterOperationToString = (reason: RosterOperation) => {
    if (reason === null) {
        return undefined;
    }

    if (reason >= 0 && reason < reasons.length) {
        return reasons[reason];
    } else {
        return undefined;
    }
}
