export type Mod = number | '-' | '+';

// 'wand-magic', 'wand', 'treasure-chest', 'swords', 'sword', 'staff', 'sparkles', 'skull-crossbones', 'sickle', 'shield-cross',
// 'scythe', 'scroll-old', 'scroll', 'ring', 'paw-claws', 'mandolin', 'mace', 'hood-cloak', 'helmet-battle',
// 'hat-wizard', 'hand-holding-magic', 'hammer-war', 'game-board-alt', 'game-board', 'flask-potion', 'flame',
// 'fist-raised', 'eye-evil', 'dungeon', 'dragon', 'dice-d8', 'dice-d6', 'dice-d4', 'dice-d20', 'dice-d12', 'dice-d10',
// 'dagger', 'campfire', 'bow-arrow', 'book-spells', 'book-dead', 'axe-battle'

export const validTags = [
    // 'skull-crossbones', 'scroll', 'ring', 'hat-wizard', 'fist-raised', 'dungeon', 'dragon', 'dice-d6', 'dice-d20', 'book-dead'
    '&#xf714;', '&#xf70e;', '&#xf70b;', '&#xf6e8;', '&#xf6de;', '&#xf6d9;', '&#xf6d5', '&#xf6d1;', '&#xf6cf;', '&#xf6b7;'
]

export class Actor {
  private _name: string;
  private _initiative: number;
  private _modifier: Mod;
  private _isPc: boolean;
  private _tag: string | undefined;

  constructor(name: string, initiative: number, modifier: Mod, isPc = false, tag = undefined) {
    this._name = name;
    this._initiative = initiative;
    this._modifier = modifier;
    this._isPc = isPc;
    this._tag = tag;
  }

  public get name(): string {
    return this._name;
  }

  public set name(name: string) {
    this._name = name;
  }

  public get initiative(): number {
    return this._initiative;
  }

  public set initiative(initiative: number) {
    this._initiative = initiative;
  }

  public get modifier(): Mod {
    return this._modifier;
  }

  public set modifier(modifier: Mod) {
    this._modifier = modifier;
  }

  public get isPc(): boolean {
    return this._isPc;
  }

  public set isPc(isPc: boolean) {
    this._isPc = isPc;
  }

  public get tag(): string {
      return this._tag ? this._tag : '';
  }

  public set tag(tag: string | undefined) {
      if (tag === undefined || validTags.includes(tag)) {
          this._tag = tag;
      }
  }
}
